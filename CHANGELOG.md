## Master branch since last release

Added features

**Implemented:**

- Add adjustment for username path references in /opt/
- Update README.md
- Add option to quit on prompts
- Update CHANGELOG.md
- Improve error handling and syntax

## 1.0 (2021-01-02)

Reworked base, added features and support for more distributions

**Implemented:**

- Change hostname
- Command option to display help
- Command option to display version
- Compatibility for Arch Linux, Fedora, Manjaro and postmarketOS
- Update description comments
- Update README.md
- Display feedback message for invalid user input
- Substitute spaces in user input with dashes
- Confirmation prompt for user input
- Check if username input exists as user on the system
- Check for operation errors
- Check if home directory needs to be renamed (Arch Linux)
- Check if "chfn" is installed before calling it (Fedora)
- Replace username in lightdm autologin.conf (Manjaro and pmOS)
- Replace username in phosh.service config (Manjaro)

## 0.2 (2020-11-27)

Added feature

**Implemented:**

- Substitute input for username to all lowercase letters

## 0.1 (2020-11-26)

Initial release

**Implemented:**

- Basic functionality to change the username on Mobian

