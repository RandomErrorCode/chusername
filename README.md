# ChUsername

An easy way to rename the default user on a Linux system.

When you flash an image of a Linux distribution to your device and this image doesn't provide any means to configure your username, you can use ChUsername to achieve that.

This tool lets you choose your desired username and will take care of everything, that is involved with replacing the current username. Additionally, you can easily change your hostname via command line option.

## Get ChUsername

Open a terminal and enter:

```
$ git clone https://gitlab.com/RandomErrorCode/chusername.git
```

This will download ChUsername to your current work directory.

## Use ChUsername

In a terminal, go into the project folder and run the tool with:

```
$ cd chusername/
$ sudo ./chusername
```

Now you can enter the username you want to replace and your name of choice. After renaming the old user you can choose to also change the hostname. Finally, ChUsername will reboot your device to apply all changes.

The intended use is after flashing a new image, but you can use ChUsername at any time on your device.

## Additional command line options

**Change hostname:**

Change only the hostname of a device.

```
$ sudo ./chusername -h
```

```
$ sudo ./chusername --host
```

**Display help:**

Display a help text and get information about command options.

```
$ ./chusername --help
```

**Display version:**

Display the version number, source repository and license.

```
$ ./chusername --version
```

## Compatibility

ChUsername was created for Mobian with Phosh for the PinePhone, but should also work on most other distributions and on different devices like the PineTab.

**Tested PinePhone distributions (with Phosh):**

- Arch Linux ARM&emsp;&nbsp;(build 2020-12-23)
- Fedora Rawhide&emsp;&nbsp;(build 2020-12-23)
- Manjaro ARM&emsp;&emsp;&nbsp;&nbsp;(build 2020-12-22)
- Mobian&emsp;&emsp;&emsp;&emsp;&emsp;(build 2020-12-22)
- postmarketOS&emsp;&emsp;(build 2020-12-23)

